<?php

namespace App\Bootstrap\Laravel\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseMiddleware;

/**
 * Class VerifyCsrfToken
 *
 * @package App\Laravel\Request\Middleware
 */
class VerifyCsrfToken extends BaseMiddleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];
}
