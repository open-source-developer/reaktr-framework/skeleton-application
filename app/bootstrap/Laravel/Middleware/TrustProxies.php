<?php

namespace App\Bootstrap\Laravel\Middleware;

use Fideloper\Proxy\TrustProxies as BaseMiddleware;
use Illuminate\Http\Request;

/**
 * Class TrustProxies
 *
 * @package App\Laravel\Request\Middleware
 */
class TrustProxies extends BaseMiddleware
{
    /**
     * The trusted proxies for this application.
     *
     * @var array|string|null
     */
    protected $proxies;

    /**
     * The headers that should be used to detect proxies.
     *
     * @var int
     */
    protected $headers = Request::HEADER_X_FORWARDED_ALL;
}
