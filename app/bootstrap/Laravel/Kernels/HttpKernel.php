<?php

namespace App\Bootstrap\Laravel\Kernels;

use Fruitcake\Cors\HandleCors;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\Http\Middleware\SetCacheHeaders;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Illuminate\Foundation\Http\Middleware\ValidatePostSize;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use App\Bootstrap\Laravel\Middleware\TrustHosts;
use App\Bootstrap\Laravel\Middleware\TrimStrings;
use App\Bootstrap\Laravel\Middleware\TrustProxies;
use Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull;
use App\Bootstrap\Laravel\Middleware\EncryptCookies;
use App\Bootstrap\Laravel\Middleware\VerifyCsrfToken;
use App\Bootstrap\Laravel\Middleware\PreventRequestsDuringMaintenance;
use OpenSourceDeveloper\Reaktr\Core\Kernels\HttpKernel as ReaktrHttpKernel;

/**
 * Class HttpKernel
 *
 * @package App\Bootstrap
 */
class HttpKernel extends ReaktrHttpKernel
{
    /**
     * global application middleware, these classes are run on every request
     *
     * @var array
     */
    protected $middleware = [
        TrustHosts::class,
        TrustProxies::class,
        HandleCors::class,
        PreventRequestsDuringMaintenance::class,
        ValidatePostSize::class,
        TrimStrings::class,
        ConvertEmptyStringsToNull::class,
    ];

    /**
     * the application's default route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        // web routing stack, this middleware is run by default on all standard HTTP requests (browser, curl etc)
        'web' => [
            EncryptCookies::class,
            AddQueuedCookiesToResponse::class,
            StartSession::class,
            ShareErrorsFromSession::class,
            VerifyCsrfToken::class,
            SubstituteBindings::class,
        ],

        // api routing stack, this middleware is run by default on all api based requests (/api/v*/* e.g /api/v1/users)
        'api' => [
            'throttle:api',
            SubstituteBindings::class,
        ],
    ];

    /**
     * application middleware classes that are available for dynamic usage on routing stacks
     *
     * @var string[]
     */
    protected $routeMiddleware = [
        'cache.headers' => SetCacheHeaders::class,
        'throttle' => ThrottleRequests::class,
    ];

}
