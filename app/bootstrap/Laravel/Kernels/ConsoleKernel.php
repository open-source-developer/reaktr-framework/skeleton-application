<?php

namespace App\Bootstrap\Laravel\Kernels;

use OpenSourceDeveloper\Reaktr\Core\Kernels\ConsoleKernel as ReaktrConsoleKernel;

/**
 * Class ConsoleKernel
 *
 * @package App\Bootstrap
 */
class ConsoleKernel extends ReaktrConsoleKernel
{
    // implement your method overrides here
}
