<?php

namespace App\Bootstrap;

use OpenSourceDeveloper\Reaktr\Core\Exceptions\Handler as ReaktrExceptionHandler;

/**
 * Class ExceptionHandler
 *
 * @package App\Bootstrap
 */
class ExceptionHandler extends ReaktrExceptionHandler
{
    // implement your method overrides here
}
