<?php

return [

    // frontend mode disables organisations, companies and api routing
    'frontend_mode_enabled' => env('REAKTR_FRONTEND_MODE_ENABLED', false),

    'auth' => [

        // lifetime in seconds for api access tokens
        'access_token_lifetime' => env('REAKTR_AUTH_ACCESS_TOKEN_LIFETIME', 600),

        // lifetime in seconds for api refresh tokens
        'refresh_token_lifetime' => env('REAKTR_AUTH_REFRESH_TOKEN_LIFETIME', 3600),

        /**
         * globally enable/disable the authentication middleware, this checks if users are authenticated
         * by the chosen authentication provider that is set within each controller
         */
        'authentication_middleware_active' => env('REAKTR_AUTH_AUTHENTICATION_MIDDLEWARE_ACTIVE', true),

        /**
         * globally enable/disable the authorisation middleware, this checks if users have the required
         * permissions to access a route, permissions for each route are specified within each controller
         */
        'authorisation_middleware_active' => env('REAKTR_AUTH_AUTHORISATION_MIDDLEWARE_ACTIVE', true),
    ],

    'database' => [
        // used when creating company databases
        'database_name_prefix' => env('REAKTR_DATABASE_NAME_PREFIX', null),
    ],

    'routing' => [

        // force the application to use https:// in urls when running behind an SSL reverse proxy
        'ssl_reverse_proxying_enabled' => env('REAKTR_SSL_REVERSE_PROXYING_ENABLED', false),

        // api specific configuration
        'api' => [

            // enable/disable the api routing system
            'enabled' => env('REAKTR_ROUTING_API_ROUTE_STACK_ENABLED', true),

            // set the prefix for api urls e.g /api
            'route_stack_prefix' => env('REAKTR_ROUTING_API_ROUTE_STACK_PREFIX', 'api'),

            // enable/disable api route versions e.g /api/v1 api/v2
            'versioning_enabled' => env('REAKTR_ROUTING_API_VERSIONING_ENABLED', true),

            // set the prefix for route versions, this will appear infront of the version number e.g "v" will result in "/v1"
            'versioning_prefix' => env('REAKTR_ROUTING_API_VERSIONING_PREFIX', 'v'),

            /*
             * set the rate limit interval, this value represents the amount of requests per metric e.g.
             * INTERVAL = 60
             * METRIC = perMinute
             * RESULT = maximum of 60 requests per minute
             */
            'rate_limit_interval' => env('REAKTR_ROUTING_API_RATE_LIMIT_INTERVAL', 60),

            // set the rate limit metric, allowed values [none, perMinute, perHour, perDay]
            'rate_limit_metric' => env('REAKTR_ROUTING_API_RATE_LIMIT_METRIC', 'perMinute'),
        ],

        'web' => [

            // enable/disable the web routing system
            'enabled' => env('REAKTR_ROUTING_WEB_ROUTE_STACK_ENABLED', true),

        ],

    ],

    'plugins' => [
        'disabled_plugins' => env('REAKTR_DISABLED_PLUGINS', null),
    ]
];
