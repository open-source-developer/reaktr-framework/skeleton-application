<?php

return [

    'resources' => [
        'company' => [
            'name' => [
                'singular' => 'company',
                'plural' => 'companies'
            ]
        ],
        'database' => [
            'name' => [
                'singular' => 'database',
                'plural' => 'databases'
            ]
        ],
        'database_host' => [
            'name' => [
                'singular' => 'database host',
                'plural' => 'database hosts'
            ]
        ],
        'domain' => [
            'name' => [
                'singular' => 'domain',
                'plural' => 'domains'
            ]
        ],
        'organisation' => [
            'name' => [
                'singular' => 'organisation',
                'plural' => 'organisations'
            ]
        ],
        'setting' => [
            'name' => [
                'singular' => 'setting',
                'plural' => 'settings'
            ]
        ],
        'setting_option' => [
            'name' => [
                'singular' => 'setting option',
                'plural' => 'setting options'
            ]
        ],
        'user' => [
            'name' => [
                'singular' => 'user',
                'plural' => 'users'
            ]
        ],
        'role' => [
            'name' => [
                'singular' => 'role',
                'plural' => 'roles'
            ]
        ],
        'permission' => [
            'name' => [
                'singular' => 'permission',
                'plural' => 'permissions'
            ]
        ]
    ],

    'permissions' => [
        'global' => [
            'unrestricted' => 'Full unrestricted access to all resources',
            'all_actions' => 'User can perform all actions on ":resource" resources',
            'list' => 'User can list ":resource" resources',
            'create' => 'User can create ":resource" resources',
            'view' => 'User can view ":resource" resources',
            'update' => 'User can update ":resource" resources',
            'delete' => 'User can delete ":resource" resources',
            'search' => 'User can search ":resource" resources',
            'attach_relation' => 'User can attach ":child" resources to ":parent" resources',
            'detach_relation' => 'User can detach ":child" resources from ":parent" resources',
        ]
    ],

    'notifications' => [
        'types' => [
            'danger' => 'danger',
            'warning' => 'warning',
            'info' => 'info',
            'success' => 'success'
        ],
    ],

    'errors' => [
        'authentication_credentials_invalid' => '',
        'authentication_account_access_prohibited' => '',
        'authentication_user_not_found' => '',
        'migration_not_found' => 'No migrations found',
        'migration_table_not_found' => 'Migration table not found',
        'migration_no_companies_found' => 'No companies were found, aborting command',
        'migration_mode_not_detected' => 'a migration mode could not be detected, this must be set in order to run migrations',
        'installation_diagnostics_failed' => 'Concierge diagnostics failed, your concierge user must have the following privileges ":privileges"',
        'installation_no_database_host_credentials' => 'Concierge credentials cannot be found for your chosen database host, please follow the steps within the framework documentation to add these credentials',
        'installation_host_connection_failed' => 'Connection to the database host was unsuccessful, please check that your concierge username and password are correct and that the user has access to your database server',
        'installation_host_privilege_retrieval_failed' => 'Database host privileges could not be determined for host, please check your database driver is supported by Reaktr',
        'installation_database_prefix_too_long' => 'Database name prefixes cannot be greater than 8 characters',
        'installation_organisation_creation_failed' => 'An error occured during creation of the organisation, check your laravel error log for more information',
        'installation_company_creation_failed' => 'An error occured during creation of the company, check your laravel error log for more information',
    ],

    'commands' => [
        'migration_seed_database_description' => 'Seed the database with records',
        'migration_seeding_complete' => 'Database seeding completed successfully',
        'migration_seeding_arg_class' => 'The class name of the root seeder',
        'migration_seeding_arg_database' => 'The database connection to seed',
        'migration_seeding_arg_force' => 'Force the operation to run when in production',
        'migration_running_command_for' => 'Running Command For',
        'migration_table_created' => 'Migration table created successfully',
        'migration_status_column_ran' => 'Ran?',
        'migration_status_column_migration' => 'Migration',
        'migration_status_column_batch' => 'Batch',
        'migration_arg_all_companies' => 'Run migrations on all company databases',
        'migration_arg_companies' => 'Run migrations on databases for given comma separated company ids',
        'plugin_cache_cleared' => 'The plugin cache was successfully cleared',
        'concierge_credentuals_updated' => 'Concierge credentials were successfully updated for host: :host',
    ],

    'installation' => [
        'title' => 'Reaktr Framework Installation Wizard',
        'pre_installation_primary_alert' => 'It is important that you have read the getting started documentation and followed the pre-installation steps at https://docs.reaktr.com before running this setup',
        'pre_installation_primary_confirmation' => 'Please confirm that you have read and completed the pre-installation steps and wish to continue with the installation',
        'previous_installation_detected_primary_alert' => 'Reaktr has already been installed, you can force a re-installation but this WILL result in full data loss on the master database',
        'previous_installation_detected_primary_confirmation' => 'Are you sure that you would like to initiate a re-installation?',
        'previous_installation_detected_secondary_alert' => 'You have triggered a re-installation, this will result in the loss of all data in the master database. Please confirm that you understand and accept that all data will be lost and that you wish to perform this action.',
        'previous_installation_detected_secondary_confirmation' => 'Type "yes" to acknowledge that you wish to wipe the master database and re-install Reaktr. This is the final confirmation, if you proceed with re-installation your data cannot be recovered',
        'master_database_pre_migrate_notice' => 'Migrating master database',
        'master_database_post_migrate_notice' => 'Master database migration completed successfully',
        'master_database_pre_seed_confirmation' => 'Would you like to seed the master database?',
        'master_database_pre_seed_notice' => 'Seeding master database',
        'master_database_post_seed_notice' => 'Master database seeding completed successfully',
        'master_database_host_alias_question' => 'What alias would you like to assign to your default database host? - defaults to:',
        'master_database_host_alias_question_default' => 'MASTER',
        'master_database_host_concierge_username_question' => 'Please enter your configured concierge username for your default host',
        'master_database_host_concierge_password_question' => 'Please enter your configured concierge password for your default host',
        'master_database_host_post_creation_notice' => 'Creation of the master database host completed successfully',
        'master_database_setup_complete' => 'Database setup is complete, would you like to add an organisation and company?',
        'organisation_name_question' => 'What would you like to call your organisation? - defaults to:',
        'organisation_name_question_default' => 'Default Organisation',
        'company_name_question' => 'What would you like to call your company? - defaults to:',
        'company_name_question_default' => 'Default Company',
        'company_custom_domain_question' => 'If you would like to add a custom domain for your company please add it here or leave blank for no domain. (format: my.company.domain - please exclude http(s)://)',
        'company_database_host_choice_question' => 'What database host do you want the company database to be created on?',
        'company_database_pre_seed_confirmation' => 'Would you like to seed the new company database?',
        'concierge_diagnostics_successful' => 'Concierge diagnostics completed successfully',
    ],

];
